# -*- coding: utf-8 -*-
# Author：十安

import os
import re


def get_ip():
    ipaddress = os.popen("ipconfig", 'r')
    for line0 in ipaddress:

        str0 = re.findall('^\s*IPv4.*', string=line0)
        if len(str0) != 0:
            result0 = re.findall(':.*\d$', str0[0])
            ip = result0[0].strip(": ")
            msg = 'IP为' + ip + '\n'
            return msg


def get_gateway():
    gatEwayAddress = os.popen("ipconfig", 'r')
    for line1 in gatEwayAddress:

        str1 = re.findall('^\s*\\u9ed8\\u8ba4\\u7f51\\u5173.*', string=line1)
        if len(str1) != 0:
            result1 = re.findall(':.*\d$', str1[0])
            try:
                gateway = result1[0].strip(": ")
            except:
                continue
            msg = '网关为' + gateway + '\n'
            return msg


def get_dns():
    dnsAddress = os.popen("ipconfig /all", 'r')
    for line2 in dnsAddress:
        str2 = re.findall('^\s*DNS.*', string=line2)

        if len(str2) != 0:
            result2 = re.findall(':.*\d$', str2[0])
            dns = result2[0].strip(": ")
            msg = 'DNS为' + dns + '\n'
            return msg
