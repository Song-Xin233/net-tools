# -*- coding: UTF8 -*-

import os
import time as t
import output as op


def setDns(net, addr):
    time = t.strftime("%H:%M:%S", t.localtime())
    msg = 'netsh interface ip set dns name="{0}" source=static addr={1} register=PRIMARY'.format(net, addr)
    os.popen(msg)
    msge = op.fix_dns_opt(addr)
    return msge

# setDns('WLAN', '114.114.114.114')
