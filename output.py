# -*- coding: utf-8 -*-
# Author：上杉夏相

import autoConnect as au
import time as t


def ipoutput():
    time = t.strftime("%H:%M:%S", t.localtime())
    ip = au.get_ip()
    msg = '[' + time + '][info]' + ip
    return msg


def gwoutput():
    time = t.strftime("%H:%M:%S", t.localtime())
    gw = au.get_gateway()
    msg = '[' + time + '][info]' + gw
    return msg


def dnsoutput():
    time = t.strftime("%H:%M:%S", t.localtime())
    dns = au.get_dns()
    msg = '[' + time + '][info]' + dns
    return msg


def fix_dns_opt(addr):
    time = t.strftime("%H:%M:%S", t.localtime())
    msge = '[' + time + '][info]您的DNS已修改为' + addr + "\n"
    msge += '[' + time + '][warn]如果未更改请尝试管理员运行'
    return msge

