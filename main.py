# -*- coding: UTF-8 -*-
# @author:上杉夏相，十安
# @version:1.0

import time as t
# 导入函数&库
import tkinter as tk

import output as o
import setDns as s
import getJiaoWu as jw

# 创建父类窗口
window = tk.Tk()
window.title('上杉的工具箱')
window.geometry('600x400')
window.iconbitmap('./favicon.ico')

# 创建框架结构
Output = tk.Frame(window, height=400, width=300)
Function = tk.Frame(window, height=200, width=300)


# 调用函数
def ipconfig():
    time = t.strftime("%H:%M:%S", t.localtime())
    msg = '[' + time + '][info]' + '您的网络信息：\n'
    ip = o.ipoutput()
    ot1.insert('end', msg)
    ot1.insert('end', ip)
    gw = o.gwoutput()
    ot1.insert('end', gw)
    dns = o.dnsoutput()
    ot1.insert('end', dns)


def fix_dns():
    fix_dnsWindow = tk.Toplevel(window)
    fix_dnsWindow.title('修改DNS')
    fix_dnsWindow.iconbitmap('./favicon.ico')

    def sure_input():
        addr = str(ie1.get())
        msg = s.setDns('WLAN', addr)
        ot1.insert('end', msg)

    ie1 = tk.Entry(fix_dnsWindow, show="None", font=('Arial', 14))
    ib1 = tk.Button(fix_dnsWindow, text='确定', font=('微软雅黑', 12), command=sure_input)
    il1 = tk.Label(fix_dnsWindow, text='输入DNS点击确定即可修改', font=('等线', 15))
    il1.pack()
    ie1.pack()
    ib1.pack(side=tk.RIGHT)
    fix_dnsWindow.mainloop()


def fix_hosts():
    time = t.strftime("%H:%M:%S", t.localtime())
    msg = '[' + time + '][warn]' + '该功能正在开发中！\n'
    ot1.insert('end', msg)


def jiao_wu():
    newWindow = tk.Toplevel(window)
    newWindow.title('教务系统')
    # newWindow.geometry('600x400')
    newWindow.iconbitmap('./favicon.ico')
    ne1 = tk.Entry(newWindow, show=None, font=('Arial', 14))
    ne2 = tk.Entry(newWindow, show="*", font=('Arial', 14))
    ne3 = tk.Entry(newWindow, show=None, font=('Arial', 14))
    ne3.insert(0, '2020-2021')
    ne4 = tk.Entry(newWindow, show=None, font=('Arial', 14))
    ne4.insert(0, '1')
    ne5 = tk.Entry(newWindow, show=None, font=('Arial', 14))
    ne5.insert(0, '查询请填写科目全称')
    nl1 = tk.Label(newWindow, text='账户', font=('等线', 15))
    nl2 = tk.Label(newWindow, text='密码', font=('等线', 15))
    nl3 = tk.Label(newWindow, text='学年', font=('等线', 15))
    nl4 = tk.Label(newWindow, text='学期', font=('等线', 15))
    nl5 = tk.Label(newWindow, text='科目', font=('等线', 15))
    ne1.grid(column=1, row=0)
    ne2.grid(column=1, row=1)
    ne3.grid(column=1, row=2)
    ne4.grid(column=1, row=3)
    ne5.grid(column=1, row=4)
    nl1.grid(column=0, row=0)
    nl2.grid(column=0, row=1)
    nl3.grid(column=0, row=2)
    nl4.grid(column=0, row=3)
    nl5.grid(column=0, row=4)

    def check_one():
        pass

    def check_all():
        myID = str(ne1.get())
        pwd = str(ne2.get())
        year = str(ne3.get())
        team = str(ne4.get())
        ls = jw.get_score(myID, pwd, year, team)
        jw.print_score(ls, year, team)
        time = t.strftime("%H:%M:%S", t.localtime())
        msg = '[' + time + '][info]'
        msg += "{}学年{}学期已打印完成".format(year, team)
        ot1.insert('end', msg)

    ib1 = tk.Button(newWindow, text='查询单科', font=('微软雅黑', 12), command=check_one)
    ib1.grid()
    ib2 = tk.Button(newWindow, text='打印全部', font=('微软雅黑', 12), command=check_all)
    ib2.grid()


def about():
    pass


# 创建GUI对象 - 功能区
ft1 = tk.Label(Function, text='功能选择区', font=('等线', 15))
fb1 = tk.Button(Function, text='获取网络信息', font=('微软雅黑', 15), command=ipconfig)
fb2 = tk.Button(Function, text='修改hosts配置', font=('微软雅黑', 15), command=fix_hosts)
fb3 = tk.Button(Function, text='教务系统', font=('微软雅黑', 15), command=jiao_wu)
fb4 = tk.Button(Function, text='修改DNS信息', font=('微软雅黑', 15), command=fix_dns)
fb5 = tk.Button(Function, text='关于', font=('微软雅黑', 15), command=about)

# 创建GUI对象 - 输入区


# 创建GUI对象 - 输出区
ot1 = tk.Text(Output, height=28, width=42)
ol1 = tk.Label(Output, text='日志输出区', font=('等线', 15))

# 放置框架结构
Output.place(x=300)
Function.place(x=80, y=13)

# 放置GUI对象 - 功能区
ft1.pack(fill='x', pady=2)
fb1.pack(fill='x', pady=2)
fb2.pack(fill='x', pady=2)
fb3.pack(fill='x', pady=2)
fb4.pack(fill='x', pady=2)
fb5.pack(fill='x', pady=2)

# 放置GUI对象 - 输出区
ot1.pack()
ol1.pack()

# 窗体循环
window.mainloop()
