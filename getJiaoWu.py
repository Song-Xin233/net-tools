# -*- coding: utf-8 -*-
# @author：上杉夏相
from school_api import SchoolClient
import xlwt


def str_score(score):
    # 格式胡成绩字符串
    ls = []
    ls += [['科目', '学分', '绩点', '正考成绩', '补考成绩', '重修成绩']]

    for className in score:
        classScore = []
        for values in className.values():
            classScore.append(str(values))
        while len(classScore) < 6:
            classScore.append("NULL")
        ls += [classScore]
    return ls


def get_all_score(myID, pwd):
    # 默认参数打印用户全部成绩
    # 暂时不可用
    # 返回成绩的二维列表
    # mySchool = SchoolClient(url='http://sys-jiaowu.nsmc.edu.cn/')
    # user = mySchool.user_login(myID, pwd)
    # score = user.get_score()
    # # print(score)
    # # 格式化一下成绩
    # ls = str_score(score)
    # return ls
    pass


def get_score(myID, pwd, year, team):
    # 默认参数打印2021学年第1学期成绩单
    # 返回成绩的二维列表
    mySchool = SchoolClient(url='http://sys-jiaowu.nsmc.edu.cn/')
    user = mySchool.user_login(myID, pwd)
    score = user.get_score(year, team)
    # print(score)
    # 格式化一下成绩
    ls = str_score(score)
    return ls


def print_score(ls, year, team):
    # 调用xlwt模块写入Excel
    index = len(ls)
    fs = xlwt.Workbook()
    sh = fs.add_sheet("成绩单")
    for i in range(index):
        for m in range(len(ls[i])):
            sh.write(i, m, ls[i][m])
    path = "./{}学年{}学期.xls".format(year, team)
    fs.save(path)
